package ViHu.Controller;

import ViHu.domain.Transaction;
import ViHu.domain.TransactionType;
import ViHu.domain.User;
import ViHu.repository.TransactionRepository;
import ViHu.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SessionScope
@EnableWebMvc
@Controller
@EnableAutoConfiguration
public class SimpleController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TransactionRepository transactionRepository;
    private User currentUser;

    @RequestMapping("/index")
    public String indexPage() {
        if (currentUser == null) return "index";
        return "redirect:/personalCab";
    }

    @RequestMapping("/")
    public String index() {
        return "redirect:/index";
    }

    @RequestMapping("/personalCab")
    public String cabinetPage(Model model) {
        try {
            if (currentUser == null) {
                return "redirect:/index";
            } else {
                List<Transaction> mt = transactionRepository.findAll();

                List<Transaction> transactionsIn = new ArrayList<>();
                List<Transaction> transactionsOut = new ArrayList<>();

                for (Transaction tr : mt) {
                    if (tr.getIdOut() == currentUser.getId()) {
                        transactionsOut.add(tr);
                    }
                    if (tr.getIdIn() == currentUser.getId()) {
                        transactionsIn.add(tr);
                    }

                }

                userOutput(currentUser, model);

                model.addAttribute("Transactions", transactionsIn);

                model.addAttribute("MutualTransactions", transactionsOut);
                return "cabinet";
            }
        } catch (NullPointerException e) {
            if (currentUser == null) {
                return "redirect:/index";
            } else {
                userOutput(currentUser, model);
                return "cabinet";
            }
        }
    }

    static void userOutput(User user, Model model) {
        model.addAttribute("name", user.getName());
        model.addAttribute("surname", user.getSurname());
        model.addAttribute("userEmail", user.getEmail());
        model.addAttribute("card", user.getCardNumber());
        model.addAttribute("balance", user.getAmount());
    }

    @RequestMapping("login")
    public String login(@RequestParam String email, @RequestParam String password, Model model) {
        if (email != null && password != null) {
            User user = userRepository.findByEmail(email);
            if (user != null && user.getPassword().equals(password)) {
                currentUser = user;
                userOutput(currentUser, model);
                return "redirect:/personalCab";
            }

        }
        return "redirect:/index";
    }

    @RequestMapping("logOut")
    public String logOut() {
        currentUser = null;
        return "redirect:/index";
    }

    @RequestMapping("registration")
    public String registration(@RequestParam String nameReg,
                               @RequestParam String surnameReg,
                               @RequestParam String emailReg,
                               @RequestParam String passwordReg,
                               Model model) {

        if (!nameReg.equals("") || !surnameReg.equals("") || !emailReg.equals("") || !passwordReg.equals("")) {
            User user = new User(nameReg, surnameReg, emailReg, passwordReg);
            user.setCardNumber(cardGenerator());
            userRepository.save(user);
            return "redirect:/index";
        }
        model.addAttribute("errorMessage", "Потрібно заповнити всі поля!");
        return "progError";
    }

    @RequestMapping("progError")
    public String simpleError() {
        return "error";
    }

    @Transactional
    @RequestMapping("transfer")
    public String transfer(@RequestParam String cardNumber, @RequestParam String transferAmount, Model model) {
        User user = userRepository.findByCardNumber(cardNumber);
        int amount = 0;
        try {
            amount = Integer.parseInt(transferAmount);
        } catch (Exception e) {
            model.addAttribute("errorMessage", "Сума введена некоректно");
            return "progError";
        }
        if (user == null) {
            model.addAttribute("errorMessage", "Карта отримувача введена неправильно!");
            return "progError";
        }

        if (currentUser.getAmount() < amount) {
            model.addAttribute("errorMessage", "Сума на вашому рахунку недостатня для здійснення переказу");
            return "progError";
        }
        updateUser(currentUser, currentUser.getAmount() - amount);
        updateUser(user, user.getAmount() + amount);
        transactionRepository.save(new Transaction(
                currentUser.getId(),
                user.getId(),
                amount,
                currentUser.getAmount(),
                user.getAmount(),
                TransactionType.CARD_TO_CARD));
        userOutput(currentUser, model);
        return "redirect:/personalCab";
    }

    @Transactional
    void updateUser(User user, int balance){
        user.setAmount(balance);
        userRepository.save(user);
    }

    @Transactional
    @RequestMapping("cellPhone")
    public String cellPhone(@RequestParam String cellPhoneTransferAmount, Model model) {
        int amount = 0;
        try {
            amount = Integer.parseInt(cellPhoneTransferAmount);
        } catch (Exception e) {
            model.addAttribute("errorMessage", "Сума введена некоректно");
        }

        if (currentUser.getAmount() < amount) {
            model.addAttribute("errorMessage", "Сума на вашому рахунку недостатня для здійснення переказу");
            return "progError";
        }
        updateUser(currentUser, currentUser.getAmount() - amount);
        userOutput(currentUser, model);
        return "redirect:/personalCab";
    }

    static String cardGenerator() {
        Random random = new Random();
        StringBuilder card = new StringBuilder();
        for (int i = 0; i < 8; i++) card.append(random.nextInt(10) + 1);
        return card.toString();
    }
}