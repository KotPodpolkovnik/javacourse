<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<HTML>
<HEAD>
    <meta charset="UTF-8">
    <%@ page contentType="text/html;charset=utf-8" %>
    <TITLE>
        MultiBank
    </TITLE>
    <link rel="shortcut icon" href="../static/static/logo.png" type="image/x-icon">
    <link href="../static/styles/style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="../static/js/jquery.js"></script>
    <script type="text/javascript" src="../static/js/script.js"></script>
</HEAD>
<BODY>
<div id="fixtitleTop">
    <table id="tableConf">
        <tr>
            <td class="col1">
                <img src="../static/static/logo.png" class="logo">
            </td>
            <td class="col2">
                <H1>MultiBank</H1>
            </td>
            <td class="col3">
                <form action="logOut" method="post">
                    <input type="submit" name="logout" value="Вихід">
                </form>
            </td>
        </tr>
    </table>
</div>

<div id="fixtitleLeft">
    Виписки за рахунками:
    <input type="button" id="MT" value="вхідні">
    <input type="button" id="PT" value="вихідні">
    <br><br>
    <input type="button" id="T" value="Переказ коштів">
    <input type="button" id="C" value="Поповнення мобільного телефону">
    <br><br>
    <input type="button" id="M" value="Персональні дані">

</div>

<div id="infofield">
    <div id="infofieldMT" class="information" hidden>
        <h2>Вхідні перекази</h2>
        <table class="tableConf">
            <tr class="trOne">
                <td>Дата</td>
                <td>Рахунок відправника</td>
                <td>Рахунок отримувача</td>
                <td>Сума транзакції</td>
                <td>Баланс</td>
            </tr>
            <c:forEach items="${MutualTransactions}" var="transactions">
                <tr>
                    <td>${transactions.date}</td>
                    <td>${transactions.idOut}</td>
                    <td>${transactions.idIn}</td>
                    <td>${transactions.amount}</td>
                    <td>${transactions.balanceOut}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <div id="infofieldPT" class="information" hidden>
        <h2>Вихідні перекази</h2>
        <table class="tableConf">
            <tr class="trOne">
                <td>Дата</td>
                <td>Рахунок відправника</td>
                <td>Рахунок отримувача</td>
                <td>Сума транзакції</td>
                <td>Баланс</td>
            </tr>
            <c:forEach items="${Transactions}" var="transactions">
                <tr>
                    <td>${transactions.date}</td>
                    <td>${transactions.idOut}</td>
                    <td>${transactions.idIn}</td>
                    <td>${transactions.amount}</td>
                    <td>${transactions.balanceIn}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <div id="infofieldT" class="information" hidden>
        <form class="tableConf" action="transfer" method="post">
            Поточний рахунок:<br>
            <p class="indexBar">${card}</p>
            <br>Введіть номер рахунку отримувача:<br>
            <input type="text" name="cardNumber" value="">
            <br>Введіть бажану суму переказу:<br>
            <input type="text" name="transferAmount" placeholder="сума">
            <br>
            <input type="submit" name="submitTransaction" value="Переказати">
        </form>
    </div>
    <div id="infofieldC" class="information" hidden>
        <form class="tableConf" action="cellPhone" method="post">
            Введіть номер телефону:<br>
            <input type="text" name="cellPhoneNumber" value="">
            <br>Введіть суму поповнення:<br>
            <input type="text" name="cellPhoneTransferAmount" placeholder="сума">
            <br>
            <input type="submit" name="submitCellphoneTransaction" value="Поповнити">
        </form>
    </div>

    <div id="infofieldM" class="information">
        <h2>Персональні дані:</h2>
        <table>
            <tr>
                <td>Ім'я:</td>
                <td>${name}</td>
            </tr>
            <tr>
                <td>Прізвище:</td>
                <td>${surname}</td>
            </tr>
            <tr>
                <td>Email:</td>
                <td>${userEmail}</td>
            </tr>
            <tr>
                <td>Персональний рахунок:</td>
                <td>
                    <table class="tableConf">
                        <tr class="trOne">
                            <td>Номер рахунку</td>
                            <td>Баланс</td>
                        </tr>
                        <tr>
                            <td>${card}</td>
                            <td>${balance}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>
<script type="text/javascript">
    $("#MT").click(function () {
        $("#infofieldMT").toggle();
    });
    $("#PT").click(function () {
        $("#infofieldPT").toggle();
    });
    $("#T").click(function () {
        $("#infofieldT").toggle();
    });
    $("#C").click(function () {
        $("#infofieldC").toggle();
    });
    $("#M").click(function () {
        $("#infofieldM").toggle();
    });
</script>
</BODY>
</HTML>
