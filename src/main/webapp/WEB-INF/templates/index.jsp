<HTML>
<HEAD>
    <meta charset="utf-8">
    <%@ page contentType="text/html;charset=utf-8" %>
    <TITLE>
        MultiBank
    </TITLE>
    <link rel="shortcut icon" href="../static/static/logo.png" type="image/x-icon">
    <link href="../static/styles/style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="../static/js/jquery.js"></script>
    <script type="text/javascript" src="../static/js/script.js"></script>
</HEAD>
<BODY>
<div id="fixtitleTop">
    <table id="tableConf">
        <tr>
            <td class="col1">
                <img src="../static/static/logo.png" class="logo">
            </td>
            <td class="col2">
                <H1>MultiBank</H1>
            </td>
        </tr>
    </table>
</div>

<div id="fixtitleLeft">
    <p class="indexBar"><br>
    <h3>Multi bank - </h3> прогресивний банк, що може допомогти вам в будь-якій ситуації.<br>
    <br>
    Спробуйте і самі переконайтесь в цьому.</p>
</div>

<div id="infofield">
    <div id="tableConf">
        <form name="login" id="log" class="login" action="login" method="post">
            Логін:<br>
            <input type="text" name="email" placeholder="email@email.com" width="50%"><br>
            Пароль:<br>
            <input type="password" name="password" width="50%"><br><br>
            <input type="submit" name="buttonLogIn" value="Вхід"><br>
            <br>Якщо ви ще не зареєстровані:
            <input type="button" id="hideLogin" value="реєстрація">
        </form>

        <form name="registration" id="reg" class="registration" action="registration" method="post" hidden>
            Ім'я: <br>
            <input type="text" name="nameReg" placeholder="Ім'я"><br>
            Прізвище: <br>
            <input type="text" name="surnameReg" placeholder="Прізвище"><br>
            Email:<br>
            <input type="text" name="emailReg" placeholder="email@email.com"><br>
            Пароль:<br>
            <input type="password" name="passwordReg" value=""><br><br>

            <input type="reset" name="reset" value="Очистити"><br><br>
            <input type="submit" name="buttonRegister" value="Зареєструватись"><br>
            <br>Повернутись до входу:
            <input type="button" id="showLogin" name="" value="Вхід">
        </form>
    </div>
</div>
<script type="text/javascript">
    $("#hideLogin").click(function () {
        $("#reg").toggle();
        $("#log").toggle();
    });
    $("#showLogin").click(function () {
        $("#reg").toggle();
        $("#log").toggle();
    });
</script>
</BODY>
</HTML>
