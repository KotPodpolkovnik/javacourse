package ViHu.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int idIn;
    private int idOut;
    private int amount;
    private int balanceIn;
    private int balanceOut;
    private Date date;
    private TransactionType type;

    public Transaction() {
    }

    public Transaction(int idIn, int idOut, int amount, int balanceIn, int balanceOut, TransactionType type) {
        this.idIn = idIn;
        this.idOut = idOut;
        this.amount = amount;
        this.date = new Date();
        this.type = type;
        this.balanceIn = balanceIn;
        this.balanceOut = balanceOut;
    }

    public int getIdIn() {
        return idIn;
    }

    public void setIdIn(int idIn) {
        this.idIn = idIn;
    }

    public int getIdOut() {
        return idOut;
    }

    public void setIdOut(int idOut) {
        this.idOut = idOut;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }
}
