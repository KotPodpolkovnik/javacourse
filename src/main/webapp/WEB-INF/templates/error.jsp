<HTML>
<HEAD>
    <meta charset="utf-8">
    <%@ page contentType="text/html;charset=utf-8" %>
    <TITLE>
        MultiBank
    </TITLE>
    <link rel="shortcut icon" href="../static/static/logo.png" type="image/x-icon">
    <link href="../static/styles/style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="../static/js/jquery.js"></script>
    <script type="text/javascript" src="../static/js/script.js"></script>
</HEAD>
<BODY>
<div id="fixtitleTop">
    <table id="tableConf">
        <tr>
            <td class="col1">
                <img src="../static/static/logo.png" class="logo">
            </td>
            <td class="col2">
                <H1>MultiBank</H1>
            </td>
        </tr>
    </table>
</div>

<div id="fixtitleLeft">
    <br>
    <h3>Multi bank - </h3>
    <p class="indexBar">
        прогресивний банк, що може допомогти вам в будь-якій ситуації.<br>
        <br>
        Спробуйте і самі переконайтесь в цьому.
    </p>
</div>

<div id="infofield">
    <H1>
    ${errorMessage}
    </H1>
</div>

<script type="text/javascript">

</script>
</BODY>
</HTML>
