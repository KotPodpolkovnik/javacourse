package ViHu.repository;

import com.sun.xml.bind.v2.model.core.ID;
import org.springframework.data.repository.CrudRepository;
import ViHu.domain.User;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByEmail(String email);
    User findByCardNumber(String card);

    void deleteUserById(int id);
}