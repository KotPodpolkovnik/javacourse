package ViHu;

import ViHu.domain.User;
import ViHu.repository.TransactionRepository;
import ViHu.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Random;

// Running application class

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner demo(final UserRepository userRepository, TransactionRepository transactionRepository) {
        return new CommandLineRunner() {
            @Override
            public void run(String... strings) throws Exception {
                for (int i = 0; i < 3; i++) {
                    User user = new User("userName" + i,
                            "userSurname" + i,
                            "userEmail" + i,
                            "user" + i);
                    user.setCardNumber(cardGenerator());
                    userRepository.save(user);
                }


            }

            String cardGenerator() {
                Random random = new Random();
                StringBuilder card = new StringBuilder();
                for (int i = 0; i < 8; i++) card.append(random.nextInt(10) + 1);
                return card.toString();
            }
        };
    }

}
